var https = require('https')
exports.handler = (event, context) => {
    // TODO implement
  try {

    if (event.session.new) {
      // New Session
      console.log("NEW SESSION")
    }
    
    var endpoint = "https://fototrack-dfb99.firebaseio.com/users/BgUvh3ubiHXvhCYoWBsiPHIqQJI3/CurrentBalance.json" // ENDPOINT GOES HERE
            var body = ""
            https.get(endpoint, (response) => {
              response.on('data', (chunk) => { body += chunk })
              response.on('end', () => {
                var data = JSON.parse(body)
                console.log(`Data:`)
                console.log( data)
                var daily = data.Daily
                var weekly = data.Weekly
                //console.log("Daily:" + daily)
                //console.log("Weekly:" + weekly)
                context.succeed(
                  generateResponse(
                    buildSpeechletResponse(`Current daily smart point balance is ${daily} and your weekly smart points balance is ${weekly}`, true),
                    {}
                  )
                )
              })
            })
      
  } catch(error) { context.fail(`Exception: ${error}`) }

  

};

// Helpers
var buildSpeechletResponse = (outputText, shouldEndSession) => {

  return {
    outputSpeech: {
      type: "PlainText",
      text: outputText
    },
    shouldEndSession: shouldEndSession
  }

}

var generateResponse = (speechletResponse, sessionAttributes) => {

  return {
    version: "1.0",
    sessionAttributes: sessionAttributes,
    response: speechletResponse
  }

}

